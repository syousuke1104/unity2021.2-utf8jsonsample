﻿#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168

namespace Utf8Json.Resolvers
{
    using System;
    using Utf8Json;

    public class GeneratedResolver : global::Utf8Json.IJsonFormatterResolver
    {
        public static readonly global::Utf8Json.IJsonFormatterResolver Instance = new GeneratedResolver();

        GeneratedResolver()
        {

        }

        public global::Utf8Json.IJsonFormatter<T> GetFormatter<T>()
        {
            return FormatterCache<T>.formatter;
        }

        static class FormatterCache<T>
        {
            public static readonly global::Utf8Json.IJsonFormatter<T> formatter;

            static FormatterCache()
            {
                var f = GeneratedResolverGetFormatterHelper.GetFormatter(typeof(T));
                if (f != null)
                {
                    formatter = (global::Utf8Json.IJsonFormatter<T>)f;
                }
            }
        }
    }

    internal static class GeneratedResolverGetFormatterHelper
    {
        static readonly global::System.Collections.Generic.Dictionary<Type, int> lookup;

        static GeneratedResolverGetFormatterHelper()
        {
            lookup = new global::System.Collections.Generic.Dictionary<Type, int>(3)
            {
                {typeof(global::Utf8JsonSample.Data.PlayerData[]), 0 },
                {typeof(global::Utf8JsonSample.Data.PlayerData), 1 },
                {typeof(global::Utf8JsonSample.Data.PlayerDataContainer), 2 },
            };
        }

        internal static object GetFormatter(Type t)
        {
            int key;
            if (!lookup.TryGetValue(t, out key)) return null;

            switch (key)
            {
                case 0: return new global::Utf8Json.Formatters.ArrayFormatter<global::Utf8JsonSample.Data.PlayerData>();
                case 1: return new Utf8Json.Formatters.Utf8JsonSample.Data.PlayerDataFormatter();
                case 2: return new Utf8Json.Formatters.Utf8JsonSample.Data.PlayerDataContainerFormatter();
                default: return null;
            }
        }
    }
}

#pragma warning disable 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612

#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 219
#pragma warning disable 168

namespace Utf8Json.Formatters.Utf8JsonSample.Data
{
    using System;
    using Utf8Json;


    public sealed class PlayerDataFormatter : global::Utf8Json.IJsonFormatter<global::Utf8JsonSample.Data.PlayerData>
    {
        readonly global::Utf8Json.Internal.AutomataDictionary ____keyMapping;
        readonly byte[][] ____stringByteKeys;

        public PlayerDataFormatter()
        {
            this.____keyMapping = new global::Utf8Json.Internal.AutomataDictionary()
            {
                { JsonWriter.GetEncodedPropertyNameWithoutQuotation("id"), 0},
                { JsonWriter.GetEncodedPropertyNameWithoutQuotation("Position"), 1},
                { JsonWriter.GetEncodedPropertyNameWithoutQuotation("Name"), 2},
                { JsonWriter.GetEncodedPropertyNameWithoutQuotation("Health"), 3},
                { JsonWriter.GetEncodedPropertyNameWithoutQuotation("Numbers"), 4},
            };

            this.____stringByteKeys = new byte[][]
            {
                JsonWriter.GetEncodedPropertyNameWithBeginObject("id"),
                JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("Position"),
                JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("Name"),
                JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("Health"),
                JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("Numbers"),
                
            };
        }

        public void Serialize(ref JsonWriter writer, global::Utf8JsonSample.Data.PlayerData value, global::Utf8Json.IJsonFormatterResolver formatterResolver)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }
            

            writer.WriteRaw(this.____stringByteKeys[0]);
            formatterResolver.GetFormatterWithVerify<global::System.Guid>().Serialize(ref writer, value.Id, formatterResolver);
            writer.WriteRaw(this.____stringByteKeys[1]);
            formatterResolver.GetFormatterWithVerify<UnityEngine.Vector3>().Serialize(ref writer, value.Position, formatterResolver);
            writer.WriteRaw(this.____stringByteKeys[2]);
            writer.WriteString(value.Name);
            writer.WriteRaw(this.____stringByteKeys[3]);
            writer.WriteSingle(value.Health);
            writer.WriteRaw(this.____stringByteKeys[4]);
            formatterResolver.GetFormatterWithVerify<int[]>().Serialize(ref writer, value.Numbers, formatterResolver);
            
            writer.WriteEndObject();
        }

        public global::Utf8JsonSample.Data.PlayerData Deserialize(ref JsonReader reader, global::Utf8Json.IJsonFormatterResolver formatterResolver)
        {
            if (reader.ReadIsNull())
            {
                return null;
            }
            

            var __Id__ = default(global::System.Guid);
            var __Id__b__ = false;
            var __Position__ = default(UnityEngine.Vector3);
            var __Position__b__ = false;
            var __Name__ = default(string);
            var __Name__b__ = false;
            var __Health__ = default(float);
            var __Health__b__ = false;
            var __Numbers__ = default(int[]);
            var __Numbers__b__ = false;

            var ____count = 0;
            reader.ReadIsBeginObjectWithVerify();
            while (!reader.ReadIsEndObjectWithSkipValueSeparator(ref ____count))
            {
                var stringKey = reader.ReadPropertyNameSegmentRaw();
                int key;
                if (!____keyMapping.TryGetValueSafe(stringKey, out key))
                {
                    reader.ReadNextBlock();
                    goto NEXT_LOOP;
                }

                switch (key)
                {
                    case 0:
                        __Id__ = formatterResolver.GetFormatterWithVerify<global::System.Guid>().Deserialize(ref reader, formatterResolver);
                        __Id__b__ = true;
                        break;
                    case 1:
                        __Position__ = formatterResolver.GetFormatterWithVerify<UnityEngine.Vector3>().Deserialize(ref reader, formatterResolver);
                        __Position__b__ = true;
                        break;
                    case 2:
                        __Name__ = reader.ReadString();
                        __Name__b__ = true;
                        break;
                    case 3:
                        __Health__ = reader.ReadSingle();
                        __Health__b__ = true;
                        break;
                    case 4:
                        __Numbers__ = formatterResolver.GetFormatterWithVerify<int[]>().Deserialize(ref reader, formatterResolver);
                        __Numbers__b__ = true;
                        break;
                    default:
                        reader.ReadNextBlock();
                        break;
                }

                NEXT_LOOP:
                continue;
            }

            var ____result = new global::Utf8JsonSample.Data.PlayerData();
            if(__Id__b__) ____result.Id = __Id__;
            if(__Position__b__) ____result.Position = __Position__;
            if(__Name__b__) ____result.Name = __Name__;
            if(__Health__b__) ____result.Health = __Health__;
            if(__Numbers__b__) ____result.Numbers = __Numbers__;

            return ____result;
        }
    }


    public sealed class PlayerDataContainerFormatter : global::Utf8Json.IJsonFormatter<global::Utf8JsonSample.Data.PlayerDataContainer>
    {
        readonly global::Utf8Json.Internal.AutomataDictionary ____keyMapping;
        readonly byte[][] ____stringByteKeys;

        public PlayerDataContainerFormatter()
        {
            this.____keyMapping = new global::Utf8Json.Internal.AutomataDictionary()
            {
                { JsonWriter.GetEncodedPropertyNameWithoutQuotation("Items"), 0},
            };

            this.____stringByteKeys = new byte[][]
            {
                JsonWriter.GetEncodedPropertyNameWithBeginObject("Items"),
                
            };
        }

        public void Serialize(ref JsonWriter writer, global::Utf8JsonSample.Data.PlayerDataContainer value, global::Utf8Json.IJsonFormatterResolver formatterResolver)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }
            

            writer.WriteRaw(this.____stringByteKeys[0]);
            formatterResolver.GetFormatterWithVerify<global::Utf8JsonSample.Data.PlayerData[]>().Serialize(ref writer, value.Items, formatterResolver);
            
            writer.WriteEndObject();
        }

        public global::Utf8JsonSample.Data.PlayerDataContainer Deserialize(ref JsonReader reader, global::Utf8Json.IJsonFormatterResolver formatterResolver)
        {
            if (reader.ReadIsNull())
            {
                return null;
            }
            

            var __Items__ = default(global::Utf8JsonSample.Data.PlayerData[]);
            var __Items__b__ = false;

            var ____count = 0;
            reader.ReadIsBeginObjectWithVerify();
            while (!reader.ReadIsEndObjectWithSkipValueSeparator(ref ____count))
            {
                var stringKey = reader.ReadPropertyNameSegmentRaw();
                int key;
                if (!____keyMapping.TryGetValueSafe(stringKey, out key))
                {
                    reader.ReadNextBlock();
                    goto NEXT_LOOP;
                }

                switch (key)
                {
                    case 0:
                        __Items__ = formatterResolver.GetFormatterWithVerify<global::Utf8JsonSample.Data.PlayerData[]>().Deserialize(ref reader, formatterResolver);
                        __Items__b__ = true;
                        break;
                    default:
                        reader.ReadNextBlock();
                        break;
                }

                NEXT_LOOP:
                continue;
            }

            var ____result = new global::Utf8JsonSample.Data.PlayerDataContainer();
            if(__Items__b__) ____result.Items = __Items__;

            return ____result;
        }
    }

}

#pragma warning disable 168
#pragma warning restore 219
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
