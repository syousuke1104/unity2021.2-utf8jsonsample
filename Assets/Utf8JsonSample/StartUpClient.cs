﻿using System;
using System.IO;
using System.Linq;
using System.Text;

using Cysharp.Threading.Tasks;

using UnityEngine;

using Utf8Json;

using Utf8JsonSample.Data;

namespace Utf8JsonSample
{
    public class StartUpClient : MonoBehaviour
    {
        private const string LogTag = nameof(StartUpClient);

        private ILogger _logger;

        private void Awake()
        {
            _logger = Debug.unityLogger;

            Utf8Json.Resolvers.CompositeResolver.RegisterAndSetAsDefault(
                Utf8Json.Resolvers.GeneratedResolver.Instance,
                Utf8Json.Resolvers.BuiltinResolver.Instance,
                Utf8Json.Unity.UnityResolver.Instance
            );

            _logger.Log(LogTag, "CompositeResolver RegisterAndSetAsDefault");
        }

        private void Start() => StartAsync().Forget();

        private async UniTaskVoid StartAsync()
        {
            var filepath = Path.Combine(Application.persistentDataPath, "playerdatalist.json");

            // Serialize process
            {
                var dataContainer = new PlayerDataContainer()
                {
                    Items = Enumerable.Range(0, 30)
                        .Select(_ => CreatePlayerData())
                        .ToArray()
                };

                using var fstream = File.Create(filepath);

                _logger.Log(LogTag, $"Serialize start. ({filepath})");

                await JsonSerializer.SerializeAsync(fstream, dataContainer);

                _logger.Log(LogTag, $"Serialize complete. ({filepath})");
            }

            // Deserialize process
            {
                using var fstream = File.OpenRead(filepath);

                _logger.Log(LogTag, $"Deserialize start. ({filepath})");

                var dataContainer = await JsonSerializer.DeserializeAsync<PlayerDataContainer>(fstream);

                _logger.Log(LogTag, $"Deserialize complete. ({filepath})");

                foreach (var data in dataContainer.Items)
                {
                    _logger.Log(Encoding.UTF8.GetString(JsonSerializer.Serialize(data)));
                }
            }
        }

        private PlayerData CreatePlayerData()
        {
            return new()
            {
                Id = Guid.NewGuid(),
                Health = UnityEngine.Random.Range(0, 100),
                Name = Convert.ToBase64String(Guid.NewGuid().ToByteArray()),
                Numbers = Enumerable.Range(0, 10).Select(_ => UnityEngine.Random.Range(0, 10)).ToArray(),
                Position = UnityEngine.Random.insideUnitSphere
            };
        }
    }
}
