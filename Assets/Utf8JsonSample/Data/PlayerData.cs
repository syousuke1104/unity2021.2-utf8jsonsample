﻿using System;
using System.Runtime.Serialization;

namespace Utf8JsonSample.Data
{
    public class PlayerDataContainer
    {
        public PlayerData[] Items { get; set; }
    }

    public class PlayerData
    {
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        public UnityEngine.Vector3 Position { get; set; }

        public string Name { get; set; }

        public float Health { get; set; }

        public int[] Numbers { get; set; }

    }
}
